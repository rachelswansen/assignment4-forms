
#Install and configure
##Import data
1. Go to the `data` directory
1. Execute `mongorestore -d cria-dev seed`


## Run the server
1. Go to the `server` directory
1. Install node modules bij executing `npm install`
1. Run the server by executing `nodemon` or `npm start`

#Your assignment

## Fix the TODO's for the next day


#Prepare a deployment

## Export data
1. Change your directory to data
1. Execute `mongodump -d s12345678 -o seed`